<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
//Route::get('/demo', 'PadronController@consulta');
Route::middleware('auth:api')->group(function () {
   
    Route::get('/charges_data', 'PadronController@charges_data');
    Route::get('/list_history', 'PadronController@list_history');
          
});

