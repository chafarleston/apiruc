<?php

namespace Modules\Services\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Services\Helpers\Cpe\DocumentValidate;
use Modules\Services\Helpers\Reniec\Dni;
use Modules\Services\Helpers\Sunat\Ruc;


class ServiceController extends Controller
{
    
    /**
     * 
     * Buscar RUC
     *
     * @param  string $number
     * @return array
     */
    public function ruc($number)
    {
        return Ruc::search($number);
    }
 
    
    /**
     * 
     * Buscar DNI
     *
     * @param  string $number
     * @return array
     */
    public function dni($number)
    {
        return Dni::search($number);
    }


    // public function documentValidate(Request $request){
 
    //     $company_number = $request->numero_ruc_emisor;
    //     $document_type_id = $request->codigo_tipo_documento;
    //     $series = $request->serie_documento;
    //     $number = $request->numero_documento;
    //     $date_of_issue = $request->fecha_de_emision;
    //     $total = $request->total;

    //     reValidate:
    //     $validate_cpe = new DocumentValidate();
    //     $response = $validate_cpe->search($company_number,$document_type_id,$series,$number,$date_of_issue,$total);

    //     if ($response['success']) {
             
    //         return [
    //             'success' => true,
    //             'data' => $response['data']
    //         ];

    //     } else {
    //         goto reValidate;
    //     }
        
    // }

}
